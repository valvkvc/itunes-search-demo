class Tunes {
    constructor() {
        this.searchInput = document.querySelector('#searchInput');
        this.searchButton = document.querySelector('#searchButton');
        this.resultsWrapper = document.querySelector('#resultsWrapper');
        this.spinner = document.querySelector('#spinner');
    }

    init() {
        this.addListeners();
    }

    addListeners() {
        this.searchButton.addEventListener('click', this.iTunesSearch);
        this.searchInput.addEventListener('keyup', this.keyPressed);
        this.searchButton.addEventListener('click', this.loading);
    }

    iTunesSearch = () => {
        this.searchITunes(this.searchInput.value);
    };

    searchITunes(searchTerm) {
        let url = this.buildITunesSearchRequestUrl(searchTerm);

        this.doHttpRequest(url, 'GET').then(
            this.processArtistSearchResults,
            this.errorHandler
        );
    }

    buildITunesSearchRequestUrl = (searchTerm) => {
        const cors = 'https://cors-anywhere.herokuapp.com/';
        const iTunesUrl = 'https://itunes.apple.com/search';
        const params = `?term=${searchTerm}&entity=musicArtist&limit=20`;
        const completeUrl = `${cors}${iTunesUrl}${params}`;

        return completeUrl;
    };

    doHttpRequest(url, method) {
        let promise = new Promise((resolve, reject) => {
            let xhr = new XMLHttpRequest();
            xhr.open(method, url, true);
            xhr.send();
            xhr.onreadystatechange = () => {
                if (xhr.readyState === 4) {
                    if (xhr.status === 200) {
                        console.log('Search successful!');
                        let response = JSON.parse(xhr.responseText);
                        resolve(response);
                    } else {
                        reject(xhr.status);
                        console.log(`Search failed!: ${xhr.statusText}`);
                    }
                }
            };
        });
        return promise;
    }

    processArtistSearchResults = (data) => {
        const results = data.results;

        if (results.length > 0) {
            let artistIds = this.buildArtistIdsArray(results);

            this.searchITunesAlbumResults(artistIds);
        } else {
            this.displayNoResultsMessage();
            this.resetSearchForm();
        }
    };

    buildArtistIdsArray = (artists) => {
        let artistIdsArray = [];
        artists.forEach((element) => {
            if (
                element.artistName
                    .toLowerCase()
                    .includes(this.searchInput.value.toLowerCase())
            ) {
                artistIdsArray.push(element.artistId);
            }
        });

        return artistIdsArray;
    };

    displayNoResultsMessage = () => {
        this.resultsWrapper.innerHTML = '';
        const message = document.createElement('div');
        message.classList.add('message');
        message.textContent = `We could't find anything for ${this.searchInput.value}. Change something and try again!`;

        this.resultsWrapper.appendChild(message);
    };

    resetSearchForm = () => {
        this.spinner.style.display = 'none';
        this.searchInput.value = '';
        this.searchButton.disabled = true;
    };

    searchITunesAlbumResults(artistIds) {
        let url = this.buildITunesLookupRequestUrl(artistIds);

        this.doHttpRequest(url, 'GET').then(
            this.processAlbumResults,
            this.errorHandler
        );
    }

    buildITunesLookupRequestUrl = (artistIds) => {
        const cors = 'https://cors-anywhere.herokuapp.com/';
        const iTunesUrl = 'https://itunes.apple.com/lookup';
        const params = `?id=${artistIds.join(',')}&entity=album&limit=200`;
        const completeUrl = `${cors}${iTunesUrl}${params}`;

        return completeUrl;
    };

    processAlbumResults = (data) => {
        this.resultsWrapper.innerHTML = '';

        const albumSearchResults = data.results;
        if (
            albumSearchResults.some(
                (element) => element.wrapperType === 'collection'
            )
        ) {
            this.displayAlbums(albumSearchResults);
        } else {
            this.displayNoResultsMessage();
        }
        this.resetSearchForm();
    };

    displayAlbums = (albumSearchResults) => {
        const searchDiv = document.createElement('div');
        searchDiv.classList.add('artistDiv');
        this.resultsWrapper.appendChild(searchDiv);

        for (let i = 0; i < albumSearchResults.length; i++) {
            if (albumSearchResults[i].wrapperType === 'collection') {
                this.buildAlbumHtml(albumSearchResults[i]);
            } else if (
                albumSearchResults[i].wrapperType === 'artist' &&
                i + 1 < albumSearchResults.length &&
                albumSearchResults[i + 1].wrapperType !== 'artist'
            ) {
                const header = document.createElement('h3');
                header.classList.add('artistHeader');
                header.textContent = albumSearchResults[i].artistName;
                this.resultsWrapper.appendChild(header);
            }
        }
    };

    buildAlbumHtml = (albumData) => {
        const div = document.createElement('div');
        div.classList.add('albumDiv');

        const artistName = document.createElement('div');
        artistName.classList.add('artistName');
        artistName.textContent = albumData.artistName;

        const image = document.createElement('img');
        image.classList.add('albumImage');
        image.src = albumData.artworkUrl100;

        const albumName = document.createElement('div');
        albumName.classList.add('albumName');
        albumName.textContent = albumData.collectionName;

        div.appendChild(artistName);
        div.appendChild(image);
        div.appendChild(albumName);
        this.resultsWrapper.appendChild(div);
    };

    errorHandler = (statusCode) => {
        console.log('Failed with status:', statusCode);
        this.resetSearchForm();

        this.resultsWrapper.innerHTML = '';
        const message = document.createElement('div');
        message.classList.add('message');
        message.textContent = 'There has been an error! Try again later!';
        this.resultsWrapper.appendChild(message);
    };

    loading = () => {
        this.spinner.style.display = 'block';
    };

    keyPressed = () => {
        if (this.searchInput.value != '') {
            this.searchButton.disabled = false; // button is enabled
            if (event.keyCode === 13) {
                // enter key pressed
                this.iTunesSearch();
                this.loading();
            }
        } else {
            this.searchButton.disabled = true; // button is disabled
        }
    };
}

const tunes = new Tunes();
window.addEventListener('load', tunes.init());
