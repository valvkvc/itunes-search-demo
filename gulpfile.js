const gulp = require('gulp'),
    terser = require('gulp-terser-js');

const minifyJS = () =>
    gulp
        .src('script.js')
        .pipe(
            terser({
                mangle: {
                    toplevel: true,
                },
            })
        )
        .on('error', function (error) {
            this.emit('end');
        })
        .pipe(gulp.dest('public/'));

exports.default = minifyJS;
